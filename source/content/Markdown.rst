Markdown syntax
================
| While creating a new proposal, you will have to add a description that will be easy to read. 
| It could be tricky, especially when you have to explain it adequately. 
| So instead of loading people with long boring descriptions, you can use markdown syntax that helps you emphasize what's more important.
| Check out markdowns supported by influence.vote down below. 

Text formatting
---------------
.. role:: strike
    :class: strike


- *italics text* - use single asterix * around: ``*italics*``
- **bold text** - use double asterix ** around: ``**bold**``
- :strike:`strikethrough` - to get text strikethrough use double tylda ~~ around it:  ``~~strikethrough~~``
- `verbatim code` - use single backquote ` around it: ```verbatim code```


Headings
---------
| To create a section or subsection within your description, you can use headings:
| **hint: the more hashes before the header, the smaller one.**

- ``# Header 1``
- ``## Header 2``
- ``### Header 3``
- ``#### Header 4``
- ``##### Header 5``
- ``###### Header 6``
   

Links
--------
| Simply pasting in the source link works within influence.vote as desired, though there is a way of formatting them better.
| Instead of long incomprehensible links, just put your link into brackets and write a short description before to format them nicely:

| This link `Description <http://your.link>`_ looks much better than http://your.link. 
| **Usage**: ``[Description](http://your.link)``



Images
--------
| Adding images into proposal is possible but limited. Markdown syntax allows to add graphics, but basics don't support size control.
| Remember, this function is not dynamic and graphics may not look the same on every device.
|
| To add an image use ``![Caption in case it didn't show](http://example.graphic.com/elephant.png)``. Remember to put an alternative description into square brackets and image address into regular ones.

Lists
------
| To create a beautifully look list with sub-items you just need to use symbols (* , +, -) and indentations for each level.

- use ``*`` for main level    
   - use ``+`` for sub-items
   - and don't forget
   - about indentations
      - sub-items of sub-items 
      - are possible
      - just use ``-`` for them
      - ...and of course a proper indentation 
- you can aways get back to the main level

Example::

    * unordered list
       + sub-item 1
       + sub-item 2
           - sub-sub-item 1
      

Tables
-------
To create a table you just need to use the following formatting. The first row content describes how column content will align, depending on using colons ``:`` ::

    | Default | Left | Right | Center |
    |---------|:-----|------:|:------:|
    | 12      | 12   |    12 |   12   |
    | 123     | 123  |   123 |  123   |
    | 1       | 1    |     1 |   1    |



Line break
-----------
| To make a line break, simply type ``---``
| It will do this:

-------------------------

| We may not have covered all the possibilities. 
| If you need anything else than listed above, try it yourself. A preview is always available underneath the proposal description during proposal creation, so if you see the desired output, you can use it, and it will work!
