Create new realm
================
| **Adding new realms is reserved for admins only.**

| To add a new realm choose ``add realm`` tile on the main influence site (if you don't see one, you don't have admin privileges).

| Add new realm form basic elements:

1. Name field
2. Networks picker
3. Strategies picker
4. Members field 
5. Logo height field
6. Logo width field
7. Realm logo uploader
8. Realm header uploader
   
| **All parameters set during creating a new realm are editable after the realm is created.** 

- You have to choose at least one network and one strategy to create a realm.

- Members field must be sent as a JSON array (at least one element)::

    ["address1","address2","address3"]

- Recommended logo height and width is 150px. Graphics size shouldn't be higher than **1 MB**. Accteptable graphic formats: **.jpg, .jpeg, .png, .webp, .svg, .gif**

- There are additional fields, that are added to the form on chains adding. Those are ``[network name] contract address`` fields that you can leave empty.


| **Only the basic form elements, listed above, are mandatory to create a new realm**