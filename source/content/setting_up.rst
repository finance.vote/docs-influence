Setting up
===========
Frontend
----------
.. _Influence: https://gitlab.com/finance.vote/influence

Repository link: `Influence`_

1. In the influence terminal::

    git checkout develop

2. In the main influence folder create **.env.local** file and put in there settings copied from **.env.example**. If you'd like to use local backend, remember to comment this line from **env.local**: ``REACT_APP_API_URL='https://test.influencebackend.xyz'``
3. Install dependencies::

    npm install

4. Run application::

    npm start


Backend
---------
.. _Influence hub: https://gitlab.com/finance.vote/influence-hub

Repository link: `Influence hub`_

1. In the influence-hub terminal::

    git checkout develop

2. Install dependencies::

    npm install
    
3. In the main influence-hub folder create **.env.local** file and put in there settings copied from **.env**.
4. Add your wallet address into ``admin_init`` file under ``migration`` folder. Make sure export line is uncommented. 
5. Run application::

    docker-compose up --build

6. If after the building there are no documents in database under strategies collection, make sure ``strategies_init`` file under ``migration`` folder has uncommented export line. 