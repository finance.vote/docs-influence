How to add submission
======================
| Tasks are special proposals type, where users know only a question and their job is to propose an answer.
| Depend on task configuration, user may be able to add more than one submission per ID (token or NFT).

| Task types:

1. text - you can add only text submissions
2. media - you can add only video, twitter or image submissions
3. mixed - you can add only text, only media or text and media at the same time 

| **Steps:**

#. Connect your wallet.
#. Enter task and read its description.
#. Hit **Create a submission** button.
#. Choose ID you want to send your proposal with.
#. Click **Next**.
#. Add **Title and content** (depend on task type).
#. Hit **Submit vote** button.
#. Confirm in MetaMask.
#. Voila! 

Text submissions
------------------
| Title and description are mandatory. 
| Description length depends on task configuration.


Media submissions
------------------
| Influence supports YouTube, Vimeo, TikTok, Twitter urls and sending images (acceptable formats: **.jpeg, .jpg, .webp, .png, .gif**)
| On submission creation, choose which media you would like to add and paste a media url or send an image. 

Mixed submissions
------------------
| This type leaves the most freedom. You can either add media or text, or both at the same time if you want. 
| The only limitation is that you have to add at least something. And a title. 