.. contents::
    :depth: 2

How To Vote
=============
#. Connect your wallet.
#. Enter proposal and read its description.
#. If needed, make sure you're on proper chain.
#. Allocate your votes (quadratic strategies) or choose an option (binary strategies).
#. Hit **Vote** button.
#. Justify your vote (optional).
#. Hit **Submit vote** button.
#. Confirm voting in MetaMask.
#. Voila! 

| If you're still confused, checkout **a quick walkthrough video** down below and `Voting Explained chapter <https://financevote.readthedocs.io/projects/influence/en/latest/content/voting.html#voting-explained>`_

.. raw:: html

    <video controls src="../_static/voting.mp4" width="700"></video>  

|

.. |votefield| image:: ../images/vote.jpg
    :width: 100px

.. admonition:: Voting notes

    To submit a vote you need to click into vote field |votefield| and type in a value or simply use arrows shown when you hover over a field. **Voting system is based on quadratic voting system, so every value typed in is squared.** This mechanism allows to vote more than once(allocate more than 1 vote) on a particular choice, but the stronger(more valuable) your votes get, the more it costs you. You can spend all of your vote power over one choice or split it over all available choices - it is up to you (but you have to distribute votes the way its total value doesn't exceed your vote power).

    For some proposals (depends on strategy used), there is a possibility of **negative voting**. If you definitely don't want particular choice to win you can try to weaken its position. Typed in value is still squaring and counted out of your vote power.

    .. image:: ../images/6_negative_vote.jpg
        :width: 70%
        :align: center


Proposal view
==================
| Main view of proposal contains a title and a description with all the information about ballot and ballot's choices. Details contain proposal hash (IPFS field), voting strategy, start and end voting date, block height (Snapshot field), countdown timer and voting result data in JSON and CSV.
| Below that, there are four tabs:

* My vote
* Vote Results
* Opinions
* Votes list (and submitted votes counter)

Tabs
------
My Vote 
^^^^^^^^^^^^^
My Vote tab contains identity picker (only in multi identity strategies proposals), account available votes, distribution of votes (allocation field) and vote form.

.. container:: templ

    .. container:: templhover

        .. figure:: ../images/1_proposal_view.png
            :width: 80%
            :target: https://financevote.readthedocs.io/projects/influence/en/latest/content/voting.html#proposal-view

    .. container:: templbasic

        .. figure:: ../images/1_proposal_view.jpg
            :width: 80%
            :target: https://financevote.readthedocs.io/projects/influence/en/latest/content/voting.html#proposal-view



Vote Results 
^^^^^^^^^^^^^^^^
| This tab shows current or final results of the ballot.
| If a winnerbox were switched 'on' during proposal creating, it will be shown here as well. 

.. figure:: ../images/2_votes_tab.jpg
    :align: center
    :width: 80%


Opinions 
^^^^^^^^^
| In the opinions tab you can see justifications for votes submitted by other users. Filtering by chocies names is a simple way to filter out only interesting you choices. 
| Sorting options works either for all opinions or filtered out choices. You can sort by oldest or newest opinions and by $I Power.

.. image:: ../images/2_opinions_tab..jpg
    :width: 80%
    :align: center

Votes list 
^^^^^^^^^^^
| Votes list tab informs how much voting power were spend by each token ID/wallet address (depending on strategy)

.. image:: ../images/5_votes_list.jpg
    :width: 80%
    :align: center


